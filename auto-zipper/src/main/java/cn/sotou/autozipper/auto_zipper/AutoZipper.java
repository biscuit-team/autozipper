package cn.sotou.autozipper.auto_zipper;

import java.io.File;

import org.zeroturnaround.zip.ZipUtil;

public class AutoZipper {

	public String unzippedDir;
	public String zippedDir;

	public AutoZipper() {
	}

	public AutoZipper(String unzip, String zipped) {
		this.unzippedDir = unzip;
		File temp = new File(unzippedDir);
		if (!temp.exists())
		{
			temp.mkdir();
		}
		this.zippedDir = zipped;
		temp = new File(zippedDir);
		if (!temp.exists())
		{
			temp.mkdir();
		}
	}

	public void zip() {
		File root = new File(unzippedDir);
		File[] files = root.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				String filename = file.getName();
				if (filename.contains("VolatileModule")) {
					String crawlerName = filename.substring(0,
							filename.length() - 14);
					String crawlerNameLowerCase = crawlerName.toLowerCase();
					File crawlerDir = new File(zippedDir + crawlerNameLowerCase
							+ "/" + crawlerNameLowerCase);
					crawlerDir.mkdir();
					File crawlerVolatileModuleDir = new File(zippedDir
							+ crawlerNameLowerCase + "/" + crawlerNameLowerCase
							+ "/CrawlerVolatileModule");
					crawlerVolatileModuleDir.mkdir();
					CopyFile copyFile = new CopyFile();
					copyFile.copyFolder(unzippedDir + crawlerName
							+ "VolatileModule", zippedDir
							+ crawlerNameLowerCase + "/" + crawlerNameLowerCase
							+ "/CrawlerVolatileModule");
					ZipUtil.pack(new File(zippedDir + crawlerNameLowerCase),
							new File(zippedDir + crawlerNameLowerCase + ".zip"));
					copyFile.delFolder(zippedDir + crawlerNameLowerCase);
				}
			}
		}
	}

}
